---
layout: post
title:  "Docker commands (draft)"
date:   2020-01-28 10:30:14 +0100
categories: cheatsheet docker
---

# Docker Commands

___Warning___: This post still needs to be edited properly. Currently it only acts as a placeholder for copy-pasting purposes. But **enjoy**!

##### Search image on Docker Hub
```
docker search ubuntu
```
#### List images and pull if needed
```
docker images
```
```
docker pull centos/centos:6.5
```
```
docker run centos:6.5 /bin/bash
```

##### List running containers

```
docker ps
```

##### List running and exited containers
```
docker ps -a
```

##### Remove image
```
docker rmi IMG_NAME
```
##### Remove image force
```
docker rmi -f IMG_NAME
```

##### Build image 
```
docker build -t="ghost" .
```
```
docker run -t -i --privileged -v /vagrant/krigoo:/ora_install_dir -p 1521:1521 --name oracle ghost /bin/bash
```
```
docker run -t -i --privileged -v /vagrant/krigoo:/ora_install_dir -p 1521:1521 -h docker_oracle --name oracle ghost /bin/bash
```
```
bash-4.1# /ora_install_dir/docker/scripts/oracle_install.sh
```

##### Build image - ignore cached containers
```
docker build --no-cache -t="ghost" .
```

##### View logs from container
```
docker logs -f oracle
```

##### Run Oracle container in daemon mode
```
docker run -d --privileged -p 1521:1521 -h docker_oracle --name oracle <host>:5000/oracle11r2 /start_oracle.sh
# Notice that -h sets constant hostname for container, --name sets constant container_name, -p --> port forwarding
```
where ```<host>``` is the hostname or the ipadress

##### Docker commit
```
docker commit -m="changed oracle startup script" -a="krigoo Sabban" a6ab7b8bb321 ghost
```

##### Docker push
```
docker tag 1eac2643c2d2 <host>:5000/oracle11r2
docker push <host>:5000/oracle11r2
```
where ```<host>``` is the hostname or the ipadress

##### Save docker image to tar ball and load image from tar
```
docker save <host>:5000/oracle11r2 > oracle11r2.tar
docker load < oracle11r2.tar
```
where ```<host>``` is the hostname or the ipadress
##### Copy saved image tar from repo and install it on local server
```
scp <host>:/root/krigoo/arc/oracle11r2.tar.gz /tmp
docker load -i /tmp/oracle11r2.tar
```
where ```<host>``` is the hostname or the ipadress
##### A quick way to recover space (cleaning old docker contaners)
```
docker rm `docker ps -a | grep Exited | awk '{print $1 }'`
docker rmi `docker images -aq`
```

##### Start docker as a daemon with http proxy settings inside
```
http_proxy=http://proxy-url:8080 /usr/bin/docker -d &
http_proxy=http://proxy-url:8080 /usr/bin/docker -d &
```
##### docker service
```
service docker start
```
##### docker daemon
```
docker -d &
```

##### Defaults
Default docker daemon parameters can be added in /etc/default/docker file.
For example to start docker with http_proxy settings add in /etc/default/docker:
```
export http_proxy="http://proxy-url:8080"
export https_proxy="http://proxy-url:8080"
```
##### Run container in interactive mode
```
docker run -t -i centos:centos6 /bin/bash
```
##### Install Docker on RHel 6.5 and above (below 7...)
```
export http_proxy=http://proxy-url:8080
export https_proxy=http://proxy-url:8080
```
###### Both proxies must be set!!!
```
wget http://mirror.nonstop.co.il/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -Uvh epel-release-6-8.noarch.rpm
yum -y install docker-io
```
##### Install Docker on Ubuntu 14.04
```
sudo apt-get update
sudo apt-get install docker.io
sudo ln -sf /usr/bin/docker.io /usr/local/bin/docker
sudo sed -i '$acomplete -F _docker docker' /etc/bash_completion.d/docker.io
source /etc/bash_completion.d/docker.io
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 36A1D7869245C8950F966E92D8576A8BA88D21E9
sudo sh -c "echo deb https://get.docker.com/ubuntu docker main\
> /etc/apt/sources.list.d/docker.list"
sudo apt-get update
sudo apt-get install lxc-docker
```
##### Test by running bash
```
docker run -i -t ubuntu /bin/bash
```
##### Add proxy settings in /etc/default/docker file and restart docker service
```
export http_proxy="http://proxy-url:8080/"
export https_proxy="http://proxy-url:8080"
service docker stop
service docker start
``` 

#### Docker.engine fails to start
If docker.engine fails to start with following message: 
Device UUID and Filesystem verification failed
Execute following
```
rm -rf /var/lib/docker/devicemapper
```
Source: <https://github.com/docker/docker/issues/23089>  

##### Install private docker registry
- Create a directory for repository configuration file
```
mkdir -p /opt/docker-registry/config
```
- Copy config_sample.yml to /opt/docker-registry/config/config.yml and change STORAGE_PATH option in *local* flavour
- Create a directory for repository data
```
mkdir -p /mnt/docker_registry
```
- Start registry container
```
docker run -d -p 5000:5000 -v /opt/docker-registry/config:/registry-conf -e DOCKER_REGISTRY_CONFIG=/registry-conf/config.yml -e SETTINGS_FLAVOR=dev -v /mnt/docker_registry:/mnt/docker_registry -e STORAGE_PATH=/mnt/docker_registry registry
```
- Check if it's running
```
docker ps -a
```
- Should be as follows:
```
CONTAINER ID IMAGE COMMAND CREATED STATUS PORTS NAMES
474a513fe825 registry:latest "/bin/sh -c 'exec do 3 minutes ago Up 3 minutes 0.0.0.0:5000->5000/tcp determined_fermat
```

##### Install private docker registry UI for searching and browsing the registry with UI
```
docker run -d -p 8081:8080 -e REG1=http://<host>:5000/v1/ atcol/docker-registry-ui
```
where ```<host>``` is the hostname or the ipadress
And browse to <registry_ui_ip>:8081 to list and search images in local docker registry
