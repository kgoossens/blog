---
layout: post
title:  "How to download from Youtube for offline use"
date:   2020-01-28 10:20:14 +0100
categories: youtube download tools tipsandtricks
---

Like music from a youtube chan, and want to be able to play it offline, or on a mp3 player (e.g. in your car, etc) ?

Use youtube-dl and ffmpeg to achieve this.

# Install ffmpeg

- [Download ffmpeg](https://ffmpeg.zeranoe.com/builds/) for windows
- extract it to a __known__ location :-)
- Add it to your "path" for easy access

e.g. on Windows 10:

* Open the Run window (right mouse click on Windows button, select _"Open Command Prompt"_, type in ```sysdm.cpl``` and press enter

![Run sysdm.cpl](/assets/images/2017-01-06-23_38_26-Run.png)

* On the next window go to the_"Advanced"_-tab and click _"Environment Variables..."_
![Advanced -> Environment Variables](/assets/images/2017-01-06-23_41_10-System-Properties.png)

* In the lower end of the screen click on _New_ and fill in the **variable name** e.g. _"ffmpeg"_ and the location of the ffmpeg \bin folder (that we just downloaded) as **variable value** and click on _OK_ to save

![Create a new variable "ffmpeg"](/assets/images/2017-01-06-23_43_34-Clipboard.png)

* Last thing we need to do is add it to our path variable. To do this _scroll down_ to find the already existing, **_path_** variable and click _Edit_. In the new screen click the _New_ button and add the newly created variable to the list e.g. _%ffmpeg%_ which corresponds with the name we've chosen in the previous step.
![Add the new variable to the path variable](/assets/images/2017-01-06-23_44_36-Edit-environment-variable.png)

Now we can use the ```ffmpeg.exe``` command wherever we want.

# Download & "install" youtube-dl on windows
* Download [youtube-dl](https://yt-dl.org/downloads/2017.01.05/youtube-dl) (check [this page](https://rg3.github.io/youtube-dl/download.html) for latest version) and save it somewhere you remember
* Add that location to the path variable, same way as shown in the ffmpeg-installation above. To speed up things, you can also choose to put youtube-dl.exe in the bin folder of ffmpeg folder. This way it's automatically added to the PATH-variable, provided you executed the ffmpeg installation above.

# Using youtube-dl to convert to mp3

Download your favorite youtube-audio track from any "Command Prompt" near you and execute ```youtube-dl``` by following this syntax:

```
youtube-dl -x --prefer-ffmpeg --audio-format mp3 <youtube-url>
```
<iframe width="560" height="315" src="https://www.youtube.com/embed/tO4dxvguQDk" frameborder="0" allowfullscreen></iframe>
e.g. to download the mp3 version of [Norah Jones - Don't Know Why
](https://youtu.be/tO4dxvguQDk) execute:

```
youtube-dl -x --prefer-ffmpeg --audio-format mp3 https://youtu.be/tO4dxvguQDk
```

The file will be downloaded, converted to mp3 and saved to the current directory.
