---
layout: post
title:  "VMware Tools Failed to install - a solution"
date:   2020-01-28 10:41:00 +0100
categories: vmware tools workaround
---

Error Shown:
_"There was a problem updating a software component. Try again later and if the problem persists, contact VMWare support or your system administrator"_

This issue is being caused by the realtime protection of your antivirus (McAfee in my case). Since we're on a corporate network, I'm not allowed to disable this protection. 
However the following __workaround__ seems to be working for me:

- __Suspend Bitlocker__ protection on your system drive.
- Run ```msconfig```, open boot tab, under boot options choose _**safe boot**_ with the _**network**_ radio button ticked.
- **Reboot** into safe mode.
- Open VMWare workstation, Edit > Preferences > Updates, and _**Download all Components Now**_. This should now work and update vmware tools as expected.
- Run ```msconfig```, open boot tab, clear the safe boot options.
- **Reboot** into "normal" windows, Bitlocker should re-enable automatically.
